import _axios, { setToken } from '@/plugins/axios'

export default {
  state: {
    access_token: '',
    refresh_token: '',
    is_authorised: false,
    sidebarVisible: true,
    sidebarUnfoldable: false,
    measureCode: 'portion',
    user: {
      role: 'guest',
    },
  },
  getters: {
    is_authorised: (state) => state.is_authorised,
    access_token: (state) => state.access_token,
    refresh_token: (state) => state.refresh_token,
    sidebarVisible: (state) => state.sidebarVisible,
    sidebarUnfoldable: (state) => state.sidebarUnfoldable,
    measureCode: (state) => state.measureCode,
    user: (state) => state.user,
  },
  actions: {
    setMeasureCode(context, payload) {
      context.commit('SET_MEASURE_CODE', payload)
    },
    login: async ({ commit }, { email, password }) => {
      let response = await _axios
        .post('login', { email, password })
        .catch((err) => {
          console.error(err)
        })
      if (response?.status === 200 && response.data.access_token) {
        commit('LOG_IN', response.data)
        setToken(response.data.access_token)
        return true
      }
      return false
    },
    logout: ({ commit }) => {
      commit('LOG_OUT')
    },
  },
  mutations: {
    SET_MEASURE_CODE: (state, measureName) => {
      state.measureCode = measureName
    },
    updateSidebarVisible(state, { value }) {
      state.sidebarVisible = value
    },
    toggleUnfoldable(state) {
      state.sidebarUnfoldable = !state.sidebarUnfoldable
    },
    toggleSidebar(state) {
      state.sidebarVisible = !state.sidebarVisible
    },
    LOG_IN(state, { access_token, refresh_token, user }) {
      state.is_authorised = true
      state.access_token = access_token
      state.refresh_token = refresh_token
      state.user = user
    },
    LOG_OUT(state) {
      state.is_authorised = false
      state.access_token = ''
      state.refresh_token = ''
      setToken('')
      _axios.get('logout')
    },
  },
}
