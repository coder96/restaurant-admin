export default {
  state: {
    qrType: 'dots',
    qrColor: '#263cad',
    stolNumber: 1,
  },
  getters: {
    qrType: (state) => state.qrType,
    qrColor: (state) => state.qrColor,
    stolNumber: (state) => state.stolNumber,
  },
  actions: {
    setQrType(context, payload) {
      console.log(payload)
      context.commit('SET_QRTYPE', payload)
    },
    setStolNumber(context, payload) {
      console.log(payload)
      context.commit('SET_STOLNUMBER', payload)
    },
    setColor(context, payload) {
      console.log(payload)
      context.commit('SET_QRCOLOR', payload)
    },
  },
  mutations: {
    SET_QRTYPE: (state, payload) => {
      state.qrType = payload
    },
    SET_STOLNUMBER: (state, payload) => {
      state.stolNumber = payload
    },
    SET_QRCOLOR: (state, payload) => {
      state.qrColor = payload
    },
  },
}
