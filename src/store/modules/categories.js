export default {
  state: {
    selectCategory: null,
  },
  getters: {
    selectCategory: (state) => state.selectCategory,
  },
  actions: {
    setSelectCategory(context, payload) {
      context.commit('SET_SELECTCATEGORY', payload)
    },
  },
  mutations: {
    SET_SELECTCATEGORY: (state, categotyId) => {
      state.selectCategory = categotyId
    },
  },
}
