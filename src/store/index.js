import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'
import app from './modules/app'
import settings from './modules/settings'
import categories from './modules/categories'

const _store = new Vuex.Store({
  state: {
    measures: [
      { code: 'gram', name: 'грам' },
      { code: 'pieces', name: 'штук' },
      { code: 'portion', name: 'порция' },
      { code: 'l', name: 'литр' },
      { code: 'ml', name: 'мл' },
      { code: '', name: '-' },
    ],
  },
  getters: {
    measures: (state) => {
      return state.measures
    },
  },
  modules: {
    app,
    settings,
    categories,
  },
  plugins: [
    createPersistedState({
      key: 'menu-admin',
    }),
  ],
})

export default _store
