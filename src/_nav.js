import { Roles } from '@/constants'

export default [
  {
    component: 'CNavItem',
    name: 'Dashboard',
    to: '/dashboard',
    icon: 'cil-speedometer',
  },
  {
    component: 'CNavTitle',
    name: 'Меню',
    role: [Roles.owner],
  },
  {
    component: 'CNavItem',
    name: 'Список',
    to: '/meals',
    icon: 'cil-list',
    role: [Roles.owner],
  },
  {
    component: 'CNavItem',
    name: 'Категории',
    to: '/categories',
    icon: 'cil-library',
    role: [Roles.owner],
  },
  {
    component: 'CNavTitle',
    name: 'Настройки',
    role: [Roles.owner],
  },
  {
    component: 'CNavItem',
    name: 'Мой росторан',
    to: '/my-restaurant',
    icon: 'cil-restaurant',
    role: [Roles.owner],
  },
  {
    component: 'CNavItem',
    name: 'Сгенерировать QR',
    to: '/generating',
    icon: 'cil-qr-code',
    role: [Roles.owner],
  },

  {
    component: 'CNavTitle',
    name: 'CRM',
    role: [Roles.superAdmin, Roles.admin],
  },
  {
    component: 'CNavItem',
    name: 'Пользователи',
    to: '/users',
    icon: 'cil-user',
    role: [Roles.superAdmin, Roles.admin],
  },
  {
    component: 'CNavItem',
    name: 'Оплаты',
    to: '/payments',
    icon: 'cil-featured-playlist',
    role: [Roles.superAdmin, Roles.admin],
  },
]
