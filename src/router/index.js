import { h, resolveComponent } from 'vue'
import { createRouter, createWebHistory } from 'vue-router'

import store from '@/store'
import DefaultLayout from '@/layouts/DefaultLayout'
import meals from './routes/meals'
import categories from './routes/categories'
import common from './routes/common'
import payments from './routes/payments'
import users from './routes/users'

const routes = [
  {
    path: '/',
    name: 'Home',
    meta: {
      title: 'Главная',
    },
    component: DefaultLayout,
    redirect: '/dashboard',
    children: [
      {
        path: '/dashboard',
        name: 'dashboard',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () =>
          import(/* webpackChunkName: "dashboard" */ '@/views/Dashboard.vue'),
      },
      ...meals,
      ...categories,
      ...common,
      ...users,
      ...payments,
    ],
  },
  {
    path: '/pages',
    redirect: '/pages/404',
    name: 'Pages',
    meta: {
      need_auth: false,
    },
    component: {
      render() {
        return h(resolveComponent('router-view'))
      },
    },
    children: [
      {
        path: '403',
        name: 'forbidden',
        component: () => import('@/views/pages/Page403'),
      },
      {
        path: '404',
        name: 'Page404',
        component: () => import('@/views/pages/Page404'),
      },
      {
        path: '500',
        name: 'Page500',
        component: () => import('@/views/pages/Page500'),
      },
      {
        path: 'login',
        name: 'Login',
        component: () => import('@/views/pages/Login'),
      },
      {
        path: 'register',
        name: 'Register',
        component: () => import('@/views/pages/Register'),
      },
    ],
  },
  {
    path: '/login',
    name: 'Login',
    meta: {
      need_auth: false,
    },
    component: () => import('@/views/Login.vue'),
  },
]

const router = createRouter({
  history: createWebHistory(process.env.VUE_APP_BASE_URL),
  routes,
  scrollBehavior() {
    // always scroll to top
    return { top: 0 }
  },
})

router.beforeEach((to) => {
  if (to.name !== 'Login' && !store.getters.access_token) {
    return {
      name: 'Login',
    }
  }
  if (!to.meta.role || to.meta.role.includes(store.getters.user.role)) {
    return true
  }
  return {
    name: 'forbidden',
  }
})

export default router
