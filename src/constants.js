export const Roles = {
  superAdmin: 'SA',
  admin: 'admin',
  owner: 'owner',
}

export const locales = ['ru', 'en']
